use std::env;
use chrono::{Local, Duration, DateTime};

#[derive(Debug)]
pub struct Email {
    pub server: String,
    pub username: String,
    pub password: String,
    pub subject: String,
    pub from: String,
    pub to: String,
}
#[derive(Debug)]
pub struct Config {
    pub baseurl: String,
    pub tls: bool,
    pub username: String,
    pub password: String,
    pub namespace: String,
    pub start: String,
    pub weekday: i8,
    pub email: Email,
    pub template: String,
    pub sidebar: String,
    pub force: bool,
    pub default_event: String,
    now: DateTime<Local>,
}

impl Config {
    pub fn new() -> Result<Config, String> {
        fn as_string(var: String) -> Result<String, String> {
            let value: Result<String, String> = match env::var(&var) {
                Ok(v) => Ok(v),
                Err(_) => Err(format!("Environment varibale '{}' is not set", var)),
            };
            return value;
        }

        fn as_bool(var: String) -> Result<bool, String> {
            let value: Result<bool, String> = match env::var(&var) {
                Ok(v) => {
                    if v == "true" || v == "True" || v == "TRUE" {
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                }
                Err(_) => Err(format!("Environment varibale {} is not set", var).to_string()),
            };
            return value;
        }

        fn as_i8(var: String) -> Result<i8, String> {
            match env::var(&var) {
                Ok(v) => {
                    match v.trim().parse() {
                        Ok(i) => return Ok(i),
                        Err(_) => return Err(format!("Unable to parse {} to i8", var)),
                    };
                }
                Err(_) => {
                    return Err(format!("Environment varibale {} is not set", var).to_string())
                }
            };
        }

        let baseurl: String = match as_string("PB_BASEURL".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let tls: bool = match as_bool("PB_TLS".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let username: String = match as_string("PB_USERNAME".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let namespace: String = match as_string("PB_NAMESPACE".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let start: String = match as_string("PB_INDEX".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let password: String = match as_string("PB_PASSWORD".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let weekday: i8 = match as_i8("PB_WEEKDAY".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let template: String = match as_string("PB_TEMPLATE".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let sidebar: String = match as_string("PB_SIDEBAR".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let default_event: String = match as_string("PB_DEFAULT_EVENT".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let force: bool = match as_bool("PB_FORCE".to_string()) {
            Ok(v) => v,
            Err(_) => {
                log::debug!("Environment variable PB_FORCE not set, using default 'false'");
                false
            }
        };
        let email_server: String = match as_string("PB_EMAIL_SERVER".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let email_username: String = match as_string("PB_EMAIL_USERNAME".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let email_password: String = match as_string("PB_EMAIL_PASSWORD".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let email_subject: String = match as_string("PB_EMAIL_SUBJECT".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let email_from: String = match as_string("PB_EMAIL_FROM".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };
        let email_to: String = match as_string("PB_EMAIL_TO".to_string()) {
            Ok(v) => v,
            Err(err) => return Err(err),
        };

        let email: Email = Email {
            server: email_server,
            username: email_username,
            password: email_password,
            subject: email_subject,
            from: email_from,
            to: email_to,
        };

        let config: Config = Config {
            baseurl,
            tls,
            username,
            password,
            email,
            start,
            namespace,
            weekday,
            template,
            sidebar,
            force,
            default_event,
            now: Local::now(),
        };

        return Ok(config);
    }
    
    pub fn api_url(&self) -> String {
        let protocol = if self.tls == true { "https" } else { "http" };
        return format!(
            "{}://{}:{}@{}/lib/exe/xmlrpc.php",
            protocol, self.username, self.password, self.baseurl
        );
    }

    fn format_date(datetime: DateTime<Local>) -> String {
        return datetime.format("%Y-%m-%d").to_string();
    }

    pub fn next_date(&self) -> DateTime<Local> {
        let current_day_of_week = self.now.format("%u").to_string().parse::<i8>().unwrap();
        let mut delta_days: i8 = self.weekday - current_day_of_week;
        if delta_days <= 0 {
            delta_days += 7;
        }
        return self.now + Duration::days(i64::from(delta_days));
    }
    
    pub fn next_date_formated(&self) -> String {
        return Self::format_date(self.next_date());
    }

    pub fn last_date(&self) -> DateTime<Local> {
        let current_day_of_week = self.now.format("%u").to_string().parse::<i8>().unwrap();
        let mut delta_days: i8 = self.weekday - current_day_of_week;
        if delta_days > 0 {
            delta_days -= 7;
        }
        return self.now + Duration::days(i64::from(delta_days));        
    }

    pub fn last_date_formated(&self) -> String {
        return Self::format_date(self.last_date());
    }

    pub fn last_page(&self) -> String {
        return format!("{}:{}", self.namespace, self.last_date_formated())
    }

    pub fn next_page(&self) -> String {
        return format!("{}:{}", self.namespace, self.next_date_formated())
    }
}
